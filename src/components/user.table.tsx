'use client'
import React, { useState } from 'react';
import { Table, Button, Modal, Input } from 'antd';
import type { TableProps } from 'antd';
import { useRouter } from 'next/navigation';

interface DataType {
    _id: string;
    name: string;
    email: number;
}


const UserTable = (props: any) => {
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [email, setEmail] = useState("");
    const router = useRouter();

    const { data } = props;
    const columns: TableProps<DataType>['columns'] = [
        {
            title: '_id',
            dataIndex: '_id',
            key: '_id',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text) => <a>{text}</a>,
        },

        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            render: (text) => <a>{text}</a>,
        }
    ];


    const handleSubmit = async () => {

        const access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0b2tlbiBsb2dpbiIsImlzcyI6ImZyb20gc2VydmVyIiwiX2lkIjoiNjRkMWM0OTYxNmE3Nzc2YjExOThiZjcxIiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJhZGRyZXNzIjoiVmlldE5hbSIsImlzVmVyaWZ5Ijp0cnVlLCJuYW1lIjoiSSdtIGFkbWluIiwidHlwZSI6IlNZU1RFTSIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTcxODI1ODExNCwiZXhwIjoxNzE4MjY4MTE0fQ.SwUvdObKYaGrWIP1d8eV8hYOZ_UfjvniofILETuShME";

        const req = await fetch("http://localhost:8000/api/v1/users", {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${access_token}`
            },
            method: "POST",
            body: JSON.stringify({
                email,
                password: "123456",
                name: "hoidanit",
                age: "25",
                role: "ADMIN",
                address: "hn",
                gender: "MALE"
            })

        })

        const d = await req.json();

        if (d.data) {
            //revalidate tags
            await fetch("/api/revalidate?tag=fetch-user", {
                headers: {
                    "Content-Type": "application/json",

                },
                method: "POST",
            })
            setOpenModal(false);
            router.refresh();
        }

    }

    return (
        <>
            <div><Button type='primary' onClick={() => setOpenModal(true)}>Add new</Button></div>
            <Table columns={columns} dataSource={data} />
            <Modal
                maskClosable={false}
                title="Basic Modal" open={openModal}
                onOk={() => handleSubmit()}
                onCancel={() => setOpenModal(false)}>
                <span>Email</span>
                <Input value={email} onChange={(e) => setEmail(e.target.value)} />
            </Modal>
        </>
    )
}

export default UserTable;