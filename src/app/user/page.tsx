import UserTable from "@/components/user.table";
interface DataType {
    key: string;
    name: string;
    age: number;
    address: string;
    tags: string[];
}


const UserPage = async () => {

    const access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0b2tlbiBsb2dpbiIsImlzcyI6ImZyb20gc2VydmVyIiwiX2lkIjoiNjRkMWM0OTYxNmE3Nzc2YjExOThiZjcxIiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJhZGRyZXNzIjoiVmlldE5hbSIsImlzVmVyaWZ5Ijp0cnVlLCJuYW1lIjoiSSdtIGFkbWluIiwidHlwZSI6IlNZU1RFTSIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTcxODI1ODExNCwiZXhwIjoxNzE4MjY4MTE0fQ.SwUvdObKYaGrWIP1d8eV8hYOZ_UfjvniofILETuShME";

    const req = await fetch("http://localhost:8000/api/v1/users/all", {
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${access_token}`
        },
        method: "GET",
        next: { tags: ["fetch-user"] }
    })

    const d = await req.json();


    return (
        <>
            <div> <h3>count user = {d?.data?.result?.length ?? 0}</h3></div>
            <div>
                <UserTable data={d.data.result} />
            </div>
        </>

    )
}

export default UserPage;